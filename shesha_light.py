#!/usr/bin/python3
#
# SheshaLite - Simple single-threaded LUKS cracker
#
# Written by Forrest Hooker, 10/05/2023

from time import sleep, perf_counter
import psutil
import subprocess
import argparse
import sys, os
import signal
import re

##  Arrs/Vars  ##

# System Info #
availThreads = os.cpu_count()

# ANSI Codes #

#Terminal Manip#
CLR = '\033[2K'
U1 = '\033[A'
D1 = '\033[B'
L_START = '\033[1000D'
LCLR = (f"{L_START}{CLR}")
#Buffer Manip#
ABUFF = '\033[?1049h'
RBUFF = '\033[?1049l'
#Cursor Manip#
HIDE_C = '\033[?25l'
SHOW_C = '\033[?25h'
START_C = '\033[H'
#Formatting#
R = '\033[0m'
B = '\033[1m'
UL = '\033[4m'
BLINK = '\033[5m'
FCL = '\033[22m'
#Colors#
RED = '\033[31m'
GRN = '\033[32m'
YEL = '\033[33m'
BLU = '\033[34m'
MAG = '\033[35m'
CYN = '\033[36m'
WHT = '\033[37m'
#Bold + Color Combos#
BU = (f"{B}{UL}") 
BR = (f"{B}{RED}") 
BG = (f"{B}{GRN}")
BY = (f"{B}{YEL}")
BB = (f"{B}{BLU}")
BM = (f"{B}{MAG}")
BC = (f"{B}{CYN}")
BW = (f"{B}{WHT}")
#BU + Color Combos#
BUW = (f"{BU}{WHT}")
#Program Header#
header = (f"{BG}Shesha (Lite) - {FCL}{WHT} Written by Forrest Hooker{R}")

# Bruteforce Vars #

#Dict of char strings
charAlph={
        "lower": "abcdefghijklmnopqrstuvwxyz",
        "upper": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "num": "123456790",
        "sym": "!?@#$%^&*)(-_+=|\\}{][\"':;/><.,~`"
        }


strIDS={
        "All": [ "all", "everything", "a", "e"],
        }


print(len(charAlph["sym"]))
#Set 'chars' as placeholder for user selection
chars = (charAlph["lower"] + charAlph["num"])
##ALL chars placeholder
#chars = (charAlph["lower"] + charAlph["upper"] + charAlph["num"] + charAlph["sym"])

#Set 'pwLen' as placeholder for user selection
pwLen = 4

# Drive/Mount Vars #
mountDir = "shesha"


##  Functions  ##


#Voodoo space magic function
def __indexToPass__(index, chars, pwLen):
    #Set 'base' to length of user selected characters
    base = len(chars)
    #Init empty list of password values (per index)
    result = [None] * pwLen
    #Loop through 'pwLen' in reverse, decrementing by 1
    for pos in range(pwLen - 1, -1, -1):
        #Set 'pos' index to divide (modulo) index of current 'char' by 'base'
        result[pos] = chars[index % base]
        #Floor divide 'index' by 'base' to update 'index' for next iteration
        index //= base
    #Return the 'result' list by joining all characters together
    return ''.join(result)


#Generate total number of passwords to try
def __passCalc__(chars, length):
    return len(chars) ** length


#The actual brute force part of the script
def __bruteForce__(drive,totalPass):
    #Init attempts counter
    attempts = 0 
    #Init Ctrl+C exit handler for quitting before runtime
    signal.signal(signal.SIGINT, __waitExitHandler__)
    #Prompt user to press enter to begin brute force
    input(f"\n\n{HIDE_C}{U1}{BR}{BLINK}PRESS [ENTER] TO BEGIN ATTACK{R}")
    #Clear input line, notify user that CTRL+C will exit during runtime
    tMain = perf_counter()
    print(f"{U1}{CLR}{BW}Press CTRL+C TO EXIT{R}")
    #Init Ctrl+C exit handler for quitting DURING runtime
    #signal.signal(signal.SIGINT, __crackExitHandler__)
    #Base decryption command using cryptsetup
    decryptCmd = ["cryptsetup", "luksOpen", drive, mountDir]
    #For every possible password length between x & y...
    for index in range(totalPass):
        #Set start time for current password attempt
        tStart = perf_counter()
        #Increment attempts counter
        attempts += 1
        #Set 'password' as every character in 'approx' joined
        password = __indexToPass__(index, chars, pwLen)
        print(f"{L_START}{WHT}Trying {password} {R} ",end='\r')
        decryptProc = subprocess.Popen(decryptCmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        #Send the password out
        stdout, stderr = decryptProc.communicate(input=f"{password}\n".encode())
        

        #If password was found...
        if decryptProc.returncode == 0:
            print(f"{GRN}{LCLR}Success with password: {password}\n")
            print(f"Total attempts: {attempts}")
            #End Timer
            tEnd = perf_counter()
            #Notify user the total time it took
            print(f"Time elapsed: {tEnd - tMain} seconds{SHOW_C}")
            return

        #If password was NOT found, continue
        if decryptProc.returncode != 0:
            #Get real-time counter for password attempt time
            tEnd = perf_counter()
            #Calculate 'tTotal'
            tTotal = tEnd - tStart
            #Notify user total time for password attempt
            print(f"                 {tTotal} seconds Elapsed",end='')
            continue
        
##  Check Functions  ##


#Function to check if user is sudo/root
def __sudoCheck__():
    #If user is not root, print error & exit
    if not os.geteuid() == 0:
        print(f"\n{BR}Run it as root\n{R}")
        sys.exit()

#Irrelevant function
def __cpuSet__():
    #Get current pid
    pid = os.getpid()
    proc = psutil.Process(pid)
    proc.cpu_affinity([0])
    os.nice(19)

#Function to parse through system-attached drives
def __attachedDrives__():
    try:
        #Base command to list all drives
        lsCmd = [ "lsblk", "-o", "TYPE,NAME" ] 
        #Grep to omit any nvme drives
        nvmePipe = [ "grep", "-v", "nvme" ]
        #Grep to omit any vgmint partitions 
        vgPipe = [ "grep", "-v", "vgmint" ] 

        ## COMMAND SECTION ##

        #Open base command, pipe into 'p2'
        p1 = subprocess.Popen(lsCmd, stdout=subprocess.PIPE)
        #Omit any nvme drives, pipe into 'p3'
        p2 = subprocess.Popen(nvmePipe, stdin=p1.stdout, stdout=subprocess.PIPE)
        p1.stdout.close()
        #Omit any vgmint parts
        p3 = subprocess.Popen(vgPipe, stdin=p2.stdout, stdout=subprocess.PIPE)
        p2.stdout.close()
        
        ## PARSE SECTION ##

        #Grab output from all commands, remove header and split by newline
        output = p3.communicate()[0].decode("utf-8").strip().split("\n")[1:]
        #Refine to ONLY /dev/sdX, remoe any partitions
        drives = [line for line in output if "disk" in line.split()[0]]
        #Grab JUST the disk name, no other information
        drive_names = [line.split()[1] for line in drives]
        #Return the list of drives 
        return drive_names
    except Exception as e:
        return f"{BR}ERROR:{FCL} {str(e)}"


#Checks attached drives for LUKS encryption
def __luksCheck__(device):
    try:
        #Run cryptsetup command to test for LUKS presence & version
        cryptCmd = ["cryptsetup", "luksDump", f"/dev/{device}"]
        #Grab the output
        cryptOutput = subprocess.run(cryptCmd, capture_output=True, text=True, check=True).stdout
        #For each line in 'cryptOutput' (split by newline)...
        for line in cryptOutput.split('\n'):
            #If Version appears anywhere in the output...
            if "Version:" in line:
                #Grab the actual version number
                version = line.split(":")[1].strip()
                #Return the version of LUKS and exit the function
                return True, f"LUKS{version}"
        return True, "Unknown LUKS Version"
    #If not LUKS enrypted, return false
    except subprocess.CalledProcessError:
        return False, "Non-LUKS drive"

#Allows user to select drives (if multiple are present)
def __driveSel__(devList):
    #Print subheader for drive selection
    print(f"{BUW}   LUKS drive(s):   {R}\n")
    #For each drive in list, print it as a numbered value
    for i, drive in enumerate(devList):
        print(f"{BW}{i+1}{FCL} - {drive}{R}")
    #Print separator 
    print(f"{BUW}                    {R}")
    try:
        #Grab user input as integer
        userDrive = int(input(f"{WHT}Please enter the number of the target drive: {R}"))
        #if userDrive is less than 1, or userDrive exceeds the length of items in devList...
        if userDrive < 1 or userDrive > len(devList):
            #Notify user of error
            print("Invalid number. Please enter a number between 1 and {}.".format(len(devList)))
            return None
        #Convert to proper list offset index
        selectedDrive = devList[userDrive - 1]
        print(f"\n{BG}{selectedDrive}{FCL} selected.{R}\n")
        return selectedDrive
    except ValueError:
        print("Invalid input. Please enter a number.")
        return None


##  Exit-Handler Functions  ##

def __waitExitHandler__(signal, frame):
    print(f"\r{CLR}{BY}Exited.{R}{SHOW_C}{L_START}")
    sys.exit()



if __name__ == "__main__":
    print(len(charAlph["sym"]))
    print(header)
    #Get euid (exit if not sudo/root)
    __sudoCheck__()
    #Get list of attached drives
    attachDrives = __attachedDrives__()
    #Init list of found luksDrives 
    luksList = []
    #If result of 'attachDrives' is an error message, print the error
    if isinstance(attachDrives, str):
        print(attachDrives)  # Print error message if any
    #Else, print the number of drives found
    else:
        print(f"\n{WHT}Found {B}{len(attachDrives)}{FCL} attached drives.{R}\n")
    #For each device in 'attachDrives' list...
    for dev in attachDrives:
        isLuks, version = __luksCheck__(dev) 
        if isLuks:
            if not version == "LUKS1":
                print(f"{RED}/dev/{dev} is Argon encrypted! Skipping.{R}")
            else:
                luksList.append(f"/dev/{dev}")
        else:
            continue
    #Have user select the drive (if multiple are present)
    targetDrive = __driveSel__(luksList)
    #Get total passwords to try
    totalPasswords = __passCalc__(chars,pwLen)
    #Initiate the attack
    __bruteForce__(targetDrive,totalPasswords)
    
