#!/usr/bin/env python3
#
# Medusa - LUKS1 (PBKDF2) Password Cracker
#
# Written by Forrest Hooker, 10/07/2023

"""
DISCLAIMER: This is inherently SLOW. All this script really is is a 
rewrite of grond.sh thrown through python. If you want quicker results,
look into JtR & Hashcat's LUKS implementations.

"""

from multiprocessing import Pool, Value, Lock
from time import sleep, perf_counter
import pyperclip
import datetime
import cpuinfo
import subprocess
import ctypes
import argparse
import os, sys
import signal


##  VARS/ARRS  ##


# ANSI Codes #

#Terminal Manip#
CLR = '\033[2K'
U1 = '\033[A'
D1 = '\033[B'
L_START = '\033[1000D'
LCLR = (f"{L_START}{CLR}")
#Buffer Manip#
ABUFF = '\033[?1049h'
RBUFF = '\033[?1049l'
#Cursor Manip#
HIDE_C = '\033[?25l'
SHOW_C = '\033[?25h'
START_C = '\033[H'
#Formatting#
R = '\033[0m'
B = '\033[1m'
UL = '\033[4m'
BLINK = '\033[5m'
FCL = '\033[22m\033[24m\033[25m'
#Colors#
RED = '\033[31m'
GRN = '\033[32m'
YEL = '\033[33m'
BLU = '\033[34m'
MAG = '\033[35m'
CYN = '\033[36m'
WHT = '\033[37m'
#Bold + Color Combos#
BU = (f"{B}{UL}")
BR = (f"{B}{RED}")
BG = (f"{B}{GRN}")
BY = (f"{B}{YEL}")
BB = (f"{B}{BLU}")
BM = (f"{B}{MAG}")
BC = (f"{B}{CYN}")
BW = (f"{B}{WHT}")
#BU + Color Combos#
BUW = (f"{BU}{WHT}")
#Program Header#
header = (f"{BG}Shesha v1.0 - {FCL}{WHT} LUKS1 (PBKDF2) Password Cracking Util{R}")
subHeader = (f"{WHT}Written by Forrest Hooker{R}")
ERR = (f"{BR}ERROR:{FCL}")

# System Info #

#Total number of threads on System
availCPUs = os.cpu_count()

# CType Values #

#Init total number of attempts as ctype integer
attempts = Value(ctypes.c_int, 0)
#Init 'found' var as False (Set as 'True' when pw found)
found = Value(ctypes.c_bool, False)
#Setup lock for atomic operation
lock = Lock()

# Brute Force Vars #


#Dict of all possible character strings
alphStr={
        "lower": "abcdefghijklmnopqrstuvwxyz",
        "upper": "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        "number": "0123456789",
        "symbol": "!?.,@#$%^&*)(-_+=][}{|\\><:;'\"~`/"
        }

#String of ALL char types + values
allStrs = ''.join(alphStr.values())
#Printable string of ALL char types + values
allStrsPrint = ("Lowercase, Uppercase, Numbers, Symbols")
#Default (Fallback) character string
defStrs = (alphStr["lower"])
#Printable string of DEFAULT char types + values
defStrsPrint = (f"Lowercase{YEL}(Defaults){R}")

# User Input Vars #

#Dict of all possible user args for setting char strings
strIDS={
        "All": [ "all", "everything", "a", "e"],
        "Default": [ "default", "standard", "normal", "d"],
        "Lower": ["lower", "lowers", "lowercase", "lowercases", "small", "smalls", "l"],
        "Upper": [ "upper", "uppers", "uppercase", "uppercases" "big", "bigs", "u"],
        "Number": [ "num", "nums", "number", "numbers", "digit", "digits", "n"],
        "Symbol": [ "sym", "syms", "symbol", "symbols", "rune", "runes", "s", "r"]
        }


strCats = {
        "lower": "Lowercase",
        "upper": "Uppercase",
        "number": "Numbers",
        "symbols": "Symbols"
        }


#Valid Delimiters for range input
rangeDelims = [
        "-",
        ",",
        ":"
        ]


# File/Mount Vars #

#Set '/dev/mapper' mount name
mountDir = "shesha"


##  FUNCTIONS  ##


# Brute Force Functions #

def __indexToPass__(index, chars, pwLen):
    result = [None] * pwLen
    for pos in range(pwLen - 1, -1, -1):
        result[pos] = chars[index % base]
        index //= base
    return ''.join(result)


def __bruteForce__(pID, start, end, pwLen):
    global targetPass
    decryptCmd = [ "cryptsetup", "luksOpen", targetDrive, mountDir ]
    for i in range(start, end):
        with lock:
            attempts.value += 1
        if found.value:
            break
        CPU = pID + 1

        password = __indexToPass__(i, chars, pwLen)
        if pID == 0:
            print(f"{WHT}CPU {CPU}: Trying {password}{R}",end='\r')
        else:
            print(f"\033[{pID}B{WHT}CPU {CPU}: Trying {password}{R}\033[{pID}A",end='\r')
        decryptProc = subprocess.Popen(decryptCmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        #Send the password out
        stdout, stderr = decryptProc.communicate(input=f"{password}\n".encode())
        
        if decryptProc.returncode == 0:
            with lock:
                found.value = True
                targetPass = password
                foundProcess = pID

                # RESULTS SECTION #

                tEnd = perf_counter()
                tTotal = str(round(tEnd - tStart, 2))
                if pID == 0:
                    print(f"{BG}CPU {CPU}:{FCL} FOUND! {BU}{targetPass}{FCL} ({tTotal} Secs Elapsed){R}\033[{numCPUs}B")
                else:
                    print(f"\033[{pID}BCPU {CPU}:{FCL} FOUND! {BU}{targetPass}{FCL} ({tTotal} Secs Elapsed){R}")
                sleep(1)
                endDown = (numCPUs - foundProcess)
                print(f"\n{SHOW_C}{BG}/dev/sdb{FCL} mounted in {UL}/dev/mapper/{mountDir}{R}")
                pyperclip.copy(targetPass)
                print(f"{WHT}Password \"{password}\" copied to clipboard{R}\n")
                break




        elif found.value:
            return





# Check Functions #

#Checks if user is sudo/root
def __sudoCheck__():
    #If user is NOT root, print error & exit
    if not os.geteuid() == 0:
        print(f"{ERR} Run it as root\n{R}")
        sys.exit()


def __argParse__():
    global targetDrive, numCPUs, cpuString, plainLen, plainStr, strAlph, strPrint

    ## Arg PARSE Section ##

    #Init Argument Parser
    parser = argparse.ArgumentParser()
    #Add argument to specify target LUKS drive
    parser.add_argument('-t', '--target',
            dest='targetDrive',
            help='Specify a target LUKS drive (in /dev/sdX format)'
    )
    #Specify number of logical procs to use
    parser.add_argument('-c', '--count',
            dest='numCPUs',
            help='Specify number of logical processors to use'
            )
    #Specify character strings to use
    parser.add_argument('-s', '--strings',
            dest='strAlph',
            nargs='+',
            help='Specify character types to use (lower,upper,number,symbol)'
            )
    parser.add_argument('-l', '--length',
            dest='plainLen',
            help='Specify a range of min-max password lengths to try(i.e. "4,256") [256 is the defined maximum]'
            )

    #Set args
    args = parser.parse_args()

    ## Arg CHECK Section ##

    #If ANY args present, set vars accordingly
    if args:
        #Target LUKS Drive
        targetDrive = args.targetDrive
        #Number of Procs to use
        numCPUs = args.numCPUs
        #Character Types
        strAlph = args.strAlph
        #Password Length Range
        plainLen = args.plainLen

    # TARGET DRIVE #

    #If no target drive given, search for it.
    if not targetDrive:
        targetDrive = __luksSearch__()
    else:
        targetDrive = __mountCheck__(targetDrive)
        

    # NUMBER OF CPUS #

    __cpuCheck__()

    if not numCPUs:
        #Set CPU count to HALF of the available CPUs
        numCPUs = (availCPUs // 2)
        cpuString = (f"{YEL}(Defaults){R}")
    else:
        numCPUs, cpuString = __procCheck__(numCPUs)

    # CHARACTER TYPES #

    if not strAlph:
        strAlph = defStrs
        strPrint = defStrsPrint
    else:
        strAlph, strPrint = __charStrCheck__(strAlph)
        print(strAlph)
        sys.exit()
    # PLAINTEXT LENGTH #

    if not plainLen:
        plainLen = range(4,9)
        plainStr = (f"4-8 Characters {YEL}(Defaults){R}")
    else:
        plainLen, plainStr = __plainLenCheck__(plainLen)

# Drive Check Functions #

def __mountCheck__(drive):
    #If user-given drive arg is not somewhere in /dev/...
    if not "/dev/sd" in drive:
        #Notify user
        print(f"{ERR} incorrect format used!{R}")
        #Print example argument
        print(f"{BY}Example drive argument:{FCL} /dev/sda{R}\n")
        #Exit
        sys.exit()
    #If the drive given does NOT exist...
    if not os.path.exists(drive):
        #Notify user
        print(f"{ERR} Drive not found! Check your physical mount point in /dev/{R}\n")
        #Exit
        sys.exit()

    #Run LUKS check & get LUKS version
    isLuks, version = __luksCheck__(drive)
    #If drive is NOT LUKS encrypted...
    if not isLuks:
        #Notify user
        print(f"{ERR} {drive} is not LUKS encrypted!{R}\n")
        #Exit
        sys.exit()
    #If drive IS LUKS encrypted...
    if isLuks:
        #If the drive is encrypted with Argon (LUKS2)...
        if not version == "LUKS1":
            #Notify user
            print(f"{ERR} {device} is {B}Argon{FCL} encrypted!{R}\n")
            #Exit
            sys.exit()
    
    #Check to make sure an existing shesha drive isn't present
    __existingDriveCheck__(targetDrive)

    return drive


def __luksSearch__():
    #Init list of found luksDrives
    luksList = []
    #Get list of attached drives
    attachDrives = __attachedDrives__()
    if isinstance(attachDrives, str):
        print(attachDrives)
    else:
        for dev in attachDrives:
            isLuks, version = __luksCheck__(dev)
            if isLuks:
                if not version == "LUKS1":
                    pass
                else:
                    luksList.append(f"/dev/{dev}")
    if not luksList:
        print(f"{ERR} No LUKS encrypted drives found! Check your mounts!{R}\n")
        sys.exit()
    elif len(luksList) == 1:
        targetDrive = luksList[0]
    else:
        targetDrive = __driveSel__(luksList)

    #Check to make sure an existing shesha drive isn't present
    __existingDriveCheck__(targetDrive)
    
    return targetDrive

#Gets list of all attached drives, filters out NVMe drives
def __attachedDrives__():
    try:
        #Base command to list all drives
        lsCmd = [ "lsblk", "-o", "TYPE,NAME" ]
        #Grep to omit any nvme drives
        nvmePipe = [ "grep", "-v", "nvme" ]
        #Grep to omit any vgmint partitions (if present)
        vgPipe = [ "grep", "-v", "vgmint" ]

        ## COMMAND SEECTION ##
        
        #Open base 'lsCmd' command, pipe into 'p2'
        p1 = subprocess.Popen(lsCmd, stdout=subprocess.PIPE)
        #Omit any nvme drives, pipe into 'p3'
        p2 = subprocess.Popen(nvmePipe, stdin=p1.stdout, stdout=subprocess.PIPE)
        p1.stdout.close()
        #Omit any vgmint partitions
        p3 = subprocess.Popen(vgPipe, stdin=p2.stdout, stdout=subprocess.PIPE)
        p2.stdout.close()

        ## PARSE SECTION ##

        #Grab output from all commands, remove header & split by newline
        output = p3.communicate()[0].decode("utf-8").strip().split("\n")[1:]
        drives = [line for line in output if "disk" in line.split()[0]]
        driveNames = [line.split()[1] for line in drives]
        return driveNames
    except Exception as e:
        return f"{BR}ERROR:{FCL} {str(e)}"


def __luksCheck__(device):
    try:
        #Run cryptsetup command to test for LUKS presence & version
        cryptCmd = ["cryptsetup", "luksDump", f"/dev/{device}"]
        #Grab the output
        cryptOutput = subprocess.run(cryptCmd, capture_output=True, text=True, check=True).stdout
        #For each line in 'cryptOutput' (split by newline)...
        for line in cryptOutput.split('\n'):
            #If Version appears anywhere in the output...
            if "Version:" in line:
                #Grab the actual version number
                version = line.split(":")[1].strip()
                #Return the version of LUKS and exit the function
                return True, f"LUKS{version}"
        return True, "Unknown LUKS Version"
    #If not LUKS enrypted, return false
    except subprocess.CalledProcessError:
        return False, "Non-LUKS drive"


def __existingDriveCheck__(targetDrive):
    if os.path.exists(f"/dev/mapper/{mountDir}"):
        print(f"{ERR} Drive is already mounted to /dev/mapper/{mountDir}!{R}\n")
        sys.exit()

def __cpuCheck__():
    print(f"{WHT}Detecting CPU...{R}",end='\r')
    cpuModel = cpuinfo.get_cpu_info()['brand_raw']
    print(f"{BW}{cpuModel}{R}")
    print(f"{BW}Available Threads:{FCL} {availCPUs}{R}\n")


#Function for validating user specified CPU count
def __procCheck__(cpuCount):

    ## ARG CHECK SECTION ##

    #If user arg for core count is not valid, notify user & exit
    if not cpuCount.isdigit() and not cpuCount in strIDS["All"]:
        print(f"{ERR}{RED} Invalid CPU count specified (Alpha character[s]). Please specify an INTEGER for total CPUs used.{R}\n")
        sys.exit()
    #Elif user has given some form of 'all as an argument...    
    elif cpuCount in strIDS["All"]:
        #Set number of CPUs to total available CPUs
        cpuCount = availCPUs
    else:
        cpuCount = int(cpuCount)

    ## ARG VAL SECTION ##

    if cpuCount > availCPUs:
        print(f"{ERR}{RED} CPU COUNT EXCEEDS AVAILABLE CPUs. Setting to {availCPUs} CPUs.{R}\n")
        cpuString = (f"{YEL}(Using {BU}ALL{FCL} Threads)")
        cpuCount = availCPUs
    elif cpuCount == availCPUs:
        cpuString = (f"{YEL}(Using {BU}ALL{FCL} Threads)")
    else:
        cpuString = ("")
    numCPUs = cpuCount
    
    return numCPUs, cpuString

#Function to check & set user defined character types 
def __charStrCheck__(usrStrs):
    #Init the FINAL Char Type Str var
    usrStrFinal = ''
    #Init list of values to print    
    usrStrPrint = []
    #Init FINAL string Var
    for string in usrStrs:

        # STRING GROUP CHECKS #

        #If user has selected ALL character types...
        if string in strIDS["All"]:
            #Set var to all char types concatenated
            usrStrs = allStrs
            #Set the print var
            usrStrPrint = allStrsPrint
            #Set warning
            strWarning = (f"{YEL}ALL STRINGS SELECTED,{BR}THIS CAN TAKE A VERY LONG TIME{FCL}")
        #Elif user has selected the DEFAULT character types...
        elif string in strIDS["Default"]:
            usrStrs = defStrs
            usrStrPrint = defStrsPrint
            strWarning = ("")
       #Elif user has entered individual character types or invalid char types...
        else:
            for category, keywords in strIDS.items():
                label = strCats.get(category)
                if label and any(s == string for s in keywords) and label not in usrStrPrint:
                    usrStrPrint.append(label)
                    usrStrFinal += alphStr[category]
            #Join str
            usrStrPrint = ','.join(usrStrPrint)

            #Placeholder value
            strWarning = ("")
        print(usrStrFinal)
        return usrStrFinal, usrStrPrint

def __plainLenCheck__(userRange):
    #Set 'plainBound' as a min/max boundary for plaintext lengths
    plainBound = range(1,256)

    ## DIGIT VERIFICATION ##

    #If user gives anything but a number...
    if any(char.isalpha() for char in userRange):
        print(f"{ERR}{RED} Invalid range specified (Non-digit char '{userRange}'){R}")
        sys.exit()
    
    ## SPLIT CHAR VERIFICATION ##

    #Else, check for valid delimiters     
    else:
        #Init boolean delim var
        delimFound = False
        #For each delimiter char in rangeDelims[]...
        for delim in rangeDelims:
            #If user-given delimiter matches 'delim'...
            if delim in userRange:
                #Set 'splitChar' as 'delim'
                splitChar = delim
                #Set found to True
                delimFound = True
                #Break the loop
                break
        #If user-given delimeter was invalid...
        if not delimFound:
            #Notify user
            print(f"{ERR}{RED} Invalid range specified (Split char is either invalid or not present in '{userRange}'{R}")
            #Print valid delimiters
            print(f"{delimHelp}\n")
            #Exit
            sys.exit()

    ## RANGE DEFINITION ##

    #Set 'start' and 'stop' values as integers split by the given split char
    startLen, stopLen = [int(i) for i in userRange.split(splitChar)]
    #If startLen is GREATER than the upper boundary, or less than 0...
    if startLen > plainBound.stop or startLen < 0:
        #Notify user
        print(f"{ERR}{RED} Invalid range start of {startLen} given. {B}Min/Max: 1/256{R}\n")
        #Exit
        sys.exit()
    #Set userRange as user defined values
    userRange = (startLen,stopLen)
    plainStr = (f"{startLen}{splitChar}{stopLen} characters")
    
    #Return the final value
    return userRange, plainStr


def __workerSetup__():
    global chars, base, tStart
    chars = strAlph
    base = len(chars)
    totalEst = len(chars)**int(plainLen[1])
    print(f"{BW}COMBINATIONS:{FCL} {totalEst} passwords to try{R}")
    print(f"{WHT}Calculating total time...{R}",end='\r')
    estTime = __estTime__(chars,plainLen[-1], totalEst)
    print(f"{L_START}{CLR}{BW}ESTIMATED TIME:{FCL} {estTime} seconds per password attempt {R}\n")
    signal.signal(signal.SIGINT, __waitExitHandler__)
    #Wait for user to press ENTER to start password run
    input(f"{HIDE_C}{BR}PRESS {BLINK}[ENTER]{FCL}{B} TO BEGIN CRACKING {FCL}{BUW}{targetDrive}{R}")
    print(f"{U1}{CLR}",end='')
    print(f"{BR}CRACKING{FCL} {BUW}{targetDrive}{R}")
    for i in range(numCPUs):
        print("\n")
    print(f"\033[{numCPUs * 2}A")
    sleep(.2)
    tStart = perf_counter()
    for passLen in plainLen:
        totalLen = len(chars)**passLen
        #Step is how we divide work amount logical procs
        step = totalLen // numCPUs
        print(f"{BW}CURRENT PASSWORD LENGTH:{FCL} {passLen}{R}")
        ranges = [(i, i+step if i+ step < totalLen else totalLen) for i in range(0, totalLen, step)]
        processIDs = list(range(len(ranges)))
        with Pool(numCPUs) as p:
            p.starmap(__bruteForce__, [(pid, start, end, passLen) for pid, (start,end) in zip(processIDs, ranges)])
        if found.value:
            break
        
##  SUB-FUNCTIONS  ##

#Literally used once lol
def Average(lst):
    return sum(lst) / len(lst)


#Password Test Time function
def __estTime__(chars,pwLen,totalLen):
    #Init the decrypt command
    decryptCmd = [ "cryptsetup", "luksOpen", targetDrive, mountDir]
    #Init our test times list
    testTimes = []
    #For i in number of CPUs...
    for i in range(numCPUs):
        #set index to i
        index = i 
        #Start the test timer
        testStart = perf_counter()
        #Init the password char list
        result = [None] * pwLen
        #For each "wheel" of the password...
        for pos in range(pwLen -1, -1, -1):
            #Set each value to it's equivalent value in chars via index % base
            result[pos] = chars[index % base]
            #Floor divide to update for the next wheel
            index //= base
        #Set password as the end result
        password = ''.join(result)
        #Init the command to send the password
        decryptProc = subprocess.Popen(decryptCmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        #Send the password 
        stdout, sderr = decryptProc.communicate(input=f"{password}\n".encode()) 
        testEnd = perf_counter()
        testTotal = testEnd - testStart
        testTimes.append(testTotal)
    testAverage = Average(testTimes)
    secTime = round(testAverage * 1)
    estTime = (f"{secTime}")
    return estTime





def __waitExitHandler__(signal, frame):
    print(f"{L_START}{CLR}{BY}Exited.{R}{SHOW_C}{L_START}")
    sys.exit()



def __infoPrint__():
    print(f"{BW}TARGET DRIVE:{FCL} {targetDrive}{R}")
    print(f"{BW}THREAD COUNT:{FCL} {numCPUs}/{availCPUs} {cpuString}{R}")
    print(f"{BW}CHARACTERS:{FCL}   {strPrint}{R}")
    print(f"{BW}PASS LENGTH:{FCL}  {plainStr}{R}\n")

if __name__ == "__main__":
    print(f"{header}\n{subHeader}\n")
    __sudoCheck__()
    __argParse__()
    __infoPrint__()
    __workerSetup__()
