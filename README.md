# Shesha - A pythonic attempt at cracking LUKS (PBKDF2) Encrypted Drives for a grade

This repo contains all of the code from my Cyber Security project at UTSA. 

## What is it?

Shesha is a multi-core program aimed at targetting specific iterations of LUKS encryption that was written for a Cyber Security class. It is the spiritual successor to my other project, Hognose, where I discovered a significantly better way of performing brute force calculations compared to itertools. Given the time-frame and nature of the class, it is not perfect. It is, unfortunately, only a slight improvement to something like *grond*, but it does function; and given enough time and enough threads, it will actually crack an encrypted drive.


## Requirements

psutil==5.9.7
py_cpuinfo==9.0.0
pyperclip==1.8.2

## Usage

Install the requirements and `chmod +x` on `shesha.py`.

Connect a LUKS (PBKDF2) encrypted drive (Hard drive, flashdrive, etc.) to your computer/server. Run `sudo ./shesha.py [args]`. 

```
Arguments:

-t - manually specify a /dev/<device> target to check/crack
-c - manually specify the number of workers to create (More is not necessarily better)
-s - manually specify the character types to use in each calculation; upper, lower, numbers, symbols
-l - manually specify the length of the password to try
```


## How does it work?

LUKS, if you are unfamiliar, is a method of encrypting entire block-devices with a master key derived from a plaintext password. It could be considered similar to Bitlocker; though Bitlocker is inherently dependent upon the presence of a TPM, and (generally) requires very little interaction from a given user. There are two available algorithms for LUKS; PBKDF2, and Argon.

Argon is outside of the scope of this project, because it adds a signficantly longer time to password attempts. PBKDF2, given a small number of iterations, is "vulnerable".

To say that LUKS is "vulnerable" is a bit like saying "Fort Knox is vulnerable because it is not comprised entirely of walls". The "vulnerability" lies in the fact that there is no limit set on password guesses, because we cannot find an entire system's resources on a hard drive. There is nothing that can hard lock the drive after a number of attempts if it is removed from the original system. However, this does not make it insecure; it is as secure as you allow it to be by how many "keys" you generate for the drive, and the plaintext content/length of the passwords the keys are derived from. This, paired with the fact that password guesses are generally very slow, allow me to confidently state that at the time of writing this program, LUKS does not have a significant vulnerability... provided you have not used a password with less than 4 characters and no unique character types, and you are not against a nation with significant compute power.


Because I am not a math genius, I did not somehow recreate the PBKDF2 algorithm within Python. What I instead did was notice the seemingly official Pip library for Cryptsetup uses a signficant number of subprocess calls to cryptsetup, and narrowed those calls down to a very simple set of lines:

```
echo "PASSWORD" | cryptsetup luksOpen /dev/<device> /dir/to/mount/drive
```

We can observe this in other programs, such as `grond`, which is a similar concept to Shesha, albeit written entirely in Bash and relying upon Dictionaries. We can run that line as many times as we want, and we'll either get a line telling us we have an invalid password, or, the drive will be mounted to our given directory.

The more complex part of this equation was already discovered in my other project, `Hognose` (A zip file cracker). How can we incrementally generate a password given a set of character types, a general idea of the length of the password, and a language that doesn't hold your hand through trying to figure out these kinds of problems? Moreover; how can me make use of this not being the 1980s, where systems have multiple cores and threads to work with?


To answer the first portion of the problem, it's easiest to consider a combination lock. How do we open a 4-Digit long, 10 character range combination lock if we don't know the combination and can't force it/break it open?


We can start by trying to identify how many TOTAL possible combinations there are, given the number of characters we have to work with, and given the length of the combination itself. Since this combination lock has 4 wheels, and we know we can try any number on each wheel from 0-9 giving us 10 posssible characters. All we do is apply this formula:

```
TC = Cr^Cl

Where:

TC = Total Combinations
Cr = Combination Range (Number of characters we can try)
Cl = Combination Length (Total length of combinations)

10^4 = 10,000

```

Now, using this, we know it will theoretically take 10,000 different wheel turns to reach the end of the possible combinations. However, this does not mean it will legitimately take 10,000 aattempts, especially if the combination was set to something easy, like 1234; which, by comparison, is much closer to a starting combination of 0-0-0-0 than the ending combination of 9-9-9-9.


Now, even though you and are able to think in an abstract sense and understand that combination 1 will be something like 0-0-0-1, we have to actually find a way to translate that to Python. The difficulty here is that rather than a dictionary attack, which iterates through lines and lines of possible passwords, we have to:

1.) Figure out how many passwords there are
2.) Iterate through that number of total passwords
3.) Translate the current index of total passwords we're on into a meaningful password
4.) Pipe that password into a function capable of spitting the value into something that accepts input

So - we have a series of nested functions and loops. Our parent function is simply counting from 0 to whatever TC was; in this case, 10,000. But in our inner function, we are calculating what that combination should be. Expressed as code, it looks something like this:

```
totalPasswords = 10,0000
chars = 10

for index in range(totalPasswords):
	password = __indexToPass__(index, chars, totalPasswords)


def __indexToPass__(index, chars, pwLen):
	#Set 'base' to length of user selected characters
    base = len(chars)
    #Init empty list of password values (per index, by INDEX)
    #WHEELS      0    1    2    3
    #result = [ " ", " ", " ", " "]
    result = [None] * pwLen
    #Loop through 'pwLen' in reverse, decrementing by 1

    """
    Range Explained (In order of appearance):
    (pwLen - 1) = Start Value. Start at pwLen minus 1.
    (-1) = Stop Value. Stop JUST BEFORE -1, or, "Stop at 0".
    (-1) = Step Value. We are saying DECREMENT by 1.
    
    COMBO LOCK WHEELS
    _______________
   |   |   |   |   |
   | 0 | 1 | 2 | 3 |
   |___|___|___|___|


    """
    #                   (3)     (0) (Down by 1)   
    for pos in range(pwLen - 1, -1, -1):
        """
        We are determining each "Wheel" (0|1|2|3) value by doing modulo division
        of index (Starting at 0/10000) by base (which is always 10).
        
        This means that the FIRST wheel comes out to 0. 

        After our index line below, the first calculated combination comes to:

        0000

        When our index becomes "1", our combination becomes:

        0001

        """
        result[pos] = chars[index % base]
        #print(f"RESULT POS{result[pos]}")
        """
        Next, we are "updating" index for the next wheel. Where wheel 0 would
        be 10,000; wheel 1 becomes 1000. Next, wheel 2 becomes 100. Finally, 
        wheel 0 becomes 10.
		"""
		index //=base
	return ''.join(result)
```

Hopefully my manic comments are satisfactory for an explanation; for every password attempt, we need to create a list of empty items equivalent to the character length of whatever our end password should be. We then need to calculate based off of our index WHAT the password should be, and we need to set each of the "wheels" to the correct value based on the index. Based off of a mod-divion operation of our index % base, we calculate each of the values for the "wheels". Then, we have to update the index and start all over again.

Now, we simply pipe that joined 'result' into our previously mentioned subprocess command and let it iterate through until we either find the password/combination, or add another "wheel"(character) onto the length of the password/combination.


However, this would be terribly, terribly slow if we tried it on a single thread. So, in order to divide the work up between other cores/threads as efficiently as possible, we need to return to our original math function and add some steps.


Let's say we have 4 friends with 4 identical locks. They all have the same combination.

All we do is divide TC by 4; and figure out who should get which combinations.


```
10^4 = 10,000

10,000 / 4 = 2500

Worker 0: Combinations 0-2500
Worker 1: Combinations 2501-5000
Worker 2: Combinations 5000-7500
Worker 3: Combinations 7500-10000

```

So, within our main function, we now distribute those individual ranges to each allocated worker via MultiProcessing's Pool & Starmap. Each worker will ONLY run through it's designated range, each checking for output equivalent to a valid return value indicating that the password worked.

**THIS IS NOT FAST.** PBKDF2, and by extension, Argon, are literally designed to thwart this kind of attack. It is cryptographically, intentionally slow, and using subprocess calls to a program of which we cannot control its resource allocation, runtime, etc; it ain't fast.


This, in my opinion, would be good as a last resort. If we could somehow remove the overhead of calling cryptsetup and run the master key hash calculation natively, *maybe* it would be faster, but probably not by much.


## Credits

I'd like to thank Professor A, who gave me an excuse to work on this (even though he knew I was going to do it anyways if it wasn't for a grade) and gave me high praise upon completion. He is one of the Professors I've encountered at UTSA that genuinely cares about this field and I am grateful to have had his class.

I would also like to thank Terry Parks, for indulging my theoretical questions and assistance.

I would definitely like to thank my father, Rodney Hooker - for encouraging my passion and walking me through things I couldn't hope to understand.

Finally, I'd like to thank Patrick; though we are no longer close, it is because of your praise and pushing me to do better that I even started down this road. Had you not seen what I couldn't, I would not have created something like this that I can truly be proud of. I only hope I can produce and create things that will make you smile in the future.
