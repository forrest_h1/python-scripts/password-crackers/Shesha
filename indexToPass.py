#!/usr/bin/python3

from time import sleep

R = '\033[0m'
#Terminal Manip#
CLR = '\033[2K'
U1 = '\033[A'
D1 = '\033[B'
L_START = '\033[1000D'
LCLR = (f"{L_START}{CLR}")




pwLen = 4
chars = "0123456789abcdefghijklmnopqrstuvwxyz"

def __passCalc__(chars,length):
    #Length of possible characters ^ the length of our password
    return len(chars) ** length


def __indexToPass__(index, chars, pwLen):
    #Set 'base' to length of user selected characters
    base = len(chars)
    #Init empty list of password values (per index, by INDEX)
    #WHEELS      0    1    2    3
    #result = [ " ", " ", " ", " "]
    result = [None] * pwLen
    #Loop through 'pwLen' in reverse, decrementing by 1

    """
    Range Explained (In order of appearance):
    (pwLen - 1) = Start Value. Start at pwLen minus 1.
    (-1) = Stop Value. Stop JUST BEFORE -1, or, "Stop at 0".
    (-1) = Step Value. We are saying DECREMENT by 1.
    
    COMBO LOCK WHEELS
    _______________
   |   |   |   |   |
   | 0 | 1 | 2 | 3 |
   |___|___|___|___|


    """
    #                   (3)     (0) (Down by 1)   
    for pos in range(pwLen - 1, -1, -1):
        """
        We are determining each "Wheel" (0|1|2|3) value by doing modulo division
        of index (Starting at 0/10000) by base (which is always 10).
        
        This means that the FIRST wheel comes out to 0. 

        After our index line below, the first calculated combination comes to:

        0000

        When our index becomes "1", our combination becomes:

        0001

        """
        result[pos] = chars[index % base]
        #print(f"RESULT POS{result[pos]}")
        """
        Next, we are "updating" index for the next wheel. Where wheel 0 would
        be 10,000; wheel 1 becomes 1000. Next, wheel 2 becomes 100. Finally, 
        wheel 0 becomes 10. 
        """
        index //= base
    print(f"{U1}",end='')
    print(f"{D1*4}RESULT: {result}{U1*3}{L_START}",end='')
    #Return the 'result' list by joining all characters together
    return '|'.join(result)


def __counter__(totalPass):
    #For each possible combination...
    for index in range(totalPass):
        #Print the index
        print(f"{L_START}CURRENT INDEX: {index}/{totalPass}{D1}",end='\r')
        #Get the password
        password = __indexToPass__(index, chars, pwLen)
        #PRINT the password
        print(f"{L_START}MAGIC LOCK SEZ: |{password}| {R} {U1}",end='\r')
        #Arbitrary "wait" to simulate password cracking load
        sleep(.5)

if __name__ == "__main__":
    print(f"TOTAL CHARACTERS TO TRY: {len(chars)}")
    print(f"LENGTH OF COMBINATION: {pwLen}")
    totalPasswords = __passCalc__(chars,pwLen)
    print(f"TOTAL COMBINATIONS: {totalPasswords}")
    __counter__(totalPasswords)


